import pygame
import sys

pygame.init()

screen = pygame.display.set_mode((500,500))
screen.fill((255,255,255))
pygame.display.set_caption("Movimiento de un círculo")

#Variables para la creación del círculo
colorNegro = (0,0,0)
posicionX = 250
posicionY = 250
velocidad = 5
radio = 80

reloj = pygame.time.Clock()

while True:
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
    
    #Código para configurar el movimiento del círculo
    movimiento = pygame.key.get_pressed()
    
    if movimiento[pygame.K_LEFT]:
        posicionX -= velocidad
    elif movimiento[pygame.K_RIGHT]:
        posicionX += velocidad
    elif movimiento[pygame.K_UP]:
        posicionY -= velocidad
    elif movimiento[pygame.K_DOWN]:
        posicionY += velocidad
        
    reloj.tick(100)
    screen.fill((255,255,255))
    circulo = pygame.draw.circle(screen, colorNegro, (posicionX, posicionY), 60)
    pygame.display.update()